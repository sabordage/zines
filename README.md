# Zines

This repository contains the PDFs for printing the zines in the Sabordage Distro (website coming soon).

In addition to a directory for each zine that is currently being printed and tabled, the repo also contains the followning subdirectories:
- workspace: zines that are currently being worked on, as well as scripts for extracting covers and guts (more info below).
- preview: zines that are ready to print but haven't been printed yet.
- archive: zines we are not planning to print for now.

The workspace directory contains `extract-covers` and `extract-guts` directories, which each contain a shell script of the same name. If a PDF is placed in one of these directories, and the script is executed, then a cover or guts PDF will be generated. The scripts depend on the pdftk (aka [pdftk-java](https://gitlab.com/pdftk-java/pdftk)) commandline tool, which is available from all linux distros, as well as homebrew/macports on mac or chocolatey/scoop on windows.

The current naming convention for the PDF directories and is based on the title of the zine (not including subtitles), written in all lower case with spaces between words replaced with dashes. For example, the zine "Another End of the World is Possible: Indigenous solidarity and blocking extractive infrastructure in Canada" becomes "another-end-of-the-world-is-possible.pdf". If it is the cover or guts file for the zines, then the end should be changed to either "-cover.pdf" or "-guts.pdf".
