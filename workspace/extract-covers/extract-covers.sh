#!/bin/bash
for filename in ./*.pdf; do
    name=$(basename $filename .pdf) &&
    echo "Extracting cover from $name.pdf and into $name-cover.pdf" && 
    pdftk "$filename" cat 1-2 output "$name-cover.pdf"
done
