#!/bin/bash
for filename in ./*.pdf; do
    name=$(basename $filename .pdf) &&
    echo "Extracting guts from $name.pdf and into $name-guts.pdf" &&
    pdftk "$filename" cat 3-end output "$name-guts.pdf"
done


